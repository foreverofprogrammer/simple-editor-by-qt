#ifndef CURSORTHREAD_H
#define CURSORTHREAD_H

#include <QThread>

class CursorThread : public QThread
{
    Q_OBJECT

public:
    CursorThread(QObject *parent = nullptr);

signals:
    void send_cursor_status(bool is_show);

private slots:
    void receive_break();

private:
    bool is_show;
    bool is_break;

protected:
    void run();
};

#endif // CURSORTHREAD_H
