#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QString>
#include <QPoint>
#include <QWheelEvent>
#include <QFile>
#include "cursorthread.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

protected:
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void inputMethodEvent(QInputMethodEvent *event);

    void wheelEvent(QWheelEvent *event);

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void send_break();

private slots:
    void receive_cursor_status(bool is_show);

    void on_action_triggered();

    void on_action_2_triggered();

    void on_action_3_triggered();

private:
    Ui::MainWindow *ui;
    CursorThread *cursor_thread;
    QString file_name;
    QFile *file;
    QPoint cursor_point;
    QPoint start_point;
    QPoint end_point;
    bool is_cursor_show;
    bool is_ctrl_down;
    bool is_column_end;
    QList<QString> line_list;
    int start_index;
    int start_column;
    int w, h;
    int max_line;
    int max_column;

};
#endif // MAINWINDOW_H
