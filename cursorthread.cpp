#include <QDebug>
#include "cursorthread.h"

CursorThread::CursorThread(QObject *parent) : QThread(parent) {
    this->is_show = false;
    this->is_break = false;
}

void CursorThread::receive_break() {
    this->is_break = true;
}

void CursorThread::run() {
    while (true) {
        if (this->is_break == true) {
            break;
        }
        if (this->is_show == true) {
            this->is_show = false;
        } else {
            this->is_show = true;
        }
        emit this->send_cursor_status(this->is_show);
        this->sleep(1);
    }
}
